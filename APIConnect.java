import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Locale;
import java.io.IOException;
import java.io.File;

public class APIConnect implements Runnable{

    URL url;
    HttpURLConnection con;
    BufferedReader reader;
    String output="",line="";

    String GenerateName(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(calendar.getTime());

        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        //int sec = calendar.get(Calendar.SECOND);

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int year = calendar.get(Calendar.YEAR);

        String month = calendar.getDisplayName(Calendar.MONTH,Calendar.LONG, Locale.getDefault());
        String result = "sk_csv_"+hours+"_"+minutes+"_"+day+"_"+month+"_"+year;

        return result;
    }

    String doInBackground(String params) {
        try{
            url = new URL(params);
            con = (HttpURLConnection) url.openConnection();
            con.connect();
            InputStream stream = con.getInputStream();
            reader = new BufferedReader(new InputStreamReader(stream));
            while((line = reader.readLine())!=null)
            {
                output = output + line;
            }
            String name_of_file = GenerateName()+".csv";
            File file = new File(name_of_file);
            //File master_file  =new File("sk_master.csv");
            RandomAccessFile master_file = new RandomAccessFile(new File("sk_master.csv"), "rw");
            
            //master_file.createNewFile();
            file.createNewFile();

            FileWriter fw  = new FileWriter(file,true);
            //FileWriter mfw = new FileWriter(master_file,true);

            JSONObject root = new JSONObject(output);
            JSONArray results = root.getJSONArray("cards");
            
            String first_result_id  = (String)results.getJSONObject(0).getString("ID");
            String last_result_id="",insertion_string="";

            //System.out.println(results.length());
            for(int i=0;i<results.length();++i){
                JSONObject current = (JSONObject) results.getJSONObject(i);
                String id= current.getString("ID");
                String post_title= current.getString("title");
                post_title = "\"" + post_title + "\"";
                String post_url= current.getString("permalink");
                String category= (String) current.getJSONArray("category").get(0);
                String no_reads= current.getString("read_count");
                fw.write(id+","+post_title+","+post_url+","+category+","+no_reads+"\n");
            }
            for(int i=0;i<results.length();++i){
                JSONObject current = (JSONObject) results.getJSONObject(i);
                String id= current.getString("ID");
                String post_title= current.getString("title");
                post_title = "\"" + post_title + "\"";
                String post_url= current.getString("permalink");
                String category= (String) current.getJSONArray("category").get(0);
                String no_reads= current.getString("read_count");
                if(id.equalsIgnoreCase(last_result_id))
                    break;
                else
                    insertion_string = insertion_string + id+","+post_title+","+post_url+","+category+","+no_reads+"\n";
            }
            master_file.seek(0);
            master_file.write(insertion_string.getBytes());
            last_result_id = first_result_id;
            System.out.println(name_of_file+" Created.");
            fw.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {
            if(con!=null)
                con.disconnect();
            try{
                if(reader!=null )
                    reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public void run() {
        doInBackground("https://login.sportskeeda.com/en/feed?page=1");
    }
}
