# SportsKeeda BackEnd

This Project Contains The Java Files for the Backend Assignment by SportsKeeda

### Prerequisites

You need to have Java Installed on Your Machine. Download JDK 1.8 and JRE Version from Oracle Website

## Deployment

Download the Respository to Your Machine and Run the "run.bat" file on Windows or run the "run.sh" on Linux Version.

## Authors

* **Prashant Mathur** 

