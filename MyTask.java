import java.util.TimerTask;

public class MyTask extends TimerTask {

    public MyTask(){
        //Some stuffs
        System.out.println("Initializing the Cron Job!");

    }

    @Override
    public void run() {
        System.out.println("Getting the Data...!!");
        Thread t = new Thread(new APIConnect());
        t.start();
        
    }

}